# Simple_web_app_on_Django
This is a small site with authors and their quotes.

To launch the application:

1. Clone this repository to your local machine

2. Install python version "^3.10"

3. Install all necessary packages and extensions. You can use poetry, or pip install -r requirements.txt

4. Go to the project folder (quotes) and
run the command "python manage.py runserver" from there in the console.

5. We go to the local host, and use the site.

The application implements authentication. Users who have registered and logged in have access to additional functionality in the form of adding new authors and new quotes.
For test:
Login - test
Password - qwerty12345678
